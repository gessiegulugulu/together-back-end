package com.tongji.together.backend.service;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import jdk.nashorn.internal.objects.annotations.Getter;
import jdk.nashorn.internal.objects.annotations.Setter;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * BaseService统一处理业务返回格式
 * @author chaoszh
 * @since 2019-11-16
 */
public class BaseService implements Serializable {
	public String successMessage(){
		JSONObject obj = new JSONObject();
		obj.put("code",200);
		obj.put("state","success");
		obj.put("time", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
		return JSON.toJSONString(obj);
	};

	public String successMessage(String data){
		JSONObject obj = new JSONObject();
		obj.put("code",200);
		obj.put("state","success");
		obj.put("data", data);
		obj.put("time", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
		return JSON.toJSONString(obj);
	}

	public String successMessage(Object dataObj){
		JSONObject obj = new JSONObject();
		obj.put("code",200);
		obj.put("state","success");
		obj.put("data", dataObj);
		obj.put("time", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
		return JSON.toJSONString(obj);
	}

	public String errorMessage(){
		JSONObject obj = new JSONObject();
		obj.put("code",500);
		obj.put("state","error");
		obj.put("time", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
		return JSON.toJSONString(obj);
	}

	public String errorMessage(String data){
		JSONObject obj = new JSONObject();
		obj.put("code",500);
		obj.put("state","error");
		obj.put("data", data);
		obj.put("time", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
		return JSON.toJSONString(obj);
	}
}
