package com.tongji.together.backend.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.JSONArray;
import com.tongji.together.backend.domain.File;
import com.tongji.together.backend.domain.Project;
import com.tongji.together.backend.domain.Task;
import com.tongji.together.backend.domain.User;
import com.tongji.together.backend.domain.relationDomain.*;
import com.tongji.together.backend.mapper.FileMapper;
import com.tongji.together.backend.mapper.ProjectMapper;
import com.tongji.together.backend.mapper.TaskMapper;
import com.tongji.together.backend.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.sql.Timestamp;

/**
 * 项目业务Service
 * @author chaoszh
 * @since 2019-11-17
 */
@Service
public class ProjectService extends BaseService {
	@Autowired
	private ProjectMapper projectMapper;
	@Autowired
	private UserMapper userMapper;
	@Autowired
	private TaskMapper taskMapper;
	@Autowired
	private FileMapper fileMapper;

	public String addProject(Project_AddProject projectAddProject){
		//add project
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		Project project = new Project(projectAddProject.projectName, timestamp);
		try{
			projectMapper.addProject(project);
		}catch (Exception e){
			System.out.println(e);
			return errorMessage("项目创建失败。");
		}
		int projectId = project.getId();
		//add project_user
		try {
			userMapper.addUserProject(projectAddProject.creatorId, projectId, true);
		}catch (Exception e){
			System.out.println(e);
			return errorMessage("项目添加创建者失败。");
		}
		return successMessage();
	}

	public String getProject(int id){
		Project project = projectMapper.getProject(id);
		if(project==null){
			return errorMessage("项目ID不存在。");
		}
		JSONArray jsonStates=JSONArray.parseArray(project.getStates());
		JSONObject jsonProject = JSONObject.parseObject(JSONObject.toJSONString(project));
		jsonProject.put("states",jsonStates);
		return successMessage(jsonProject);
	}

	public String updateProject(int id, Project_UpdateProject projectUpdateProject){
		Project project = new Project(
				projectUpdateProject.projectName,
				JSONObject.toJSONString(projectUpdateProject.states),
				new Timestamp(System.currentTimeMillis())
		);
		projectMapper.updateProject(id,project);
		return successMessage();
	}

	public String removeProject(int id){
		projectMapper.removeProject(id);
		//remove project user
		projectMapper.removeProjectUser(id);
		return successMessage();
	}

	public String getProjectUser(int id){
		List<Project_GetProjectUser> users = projectMapper.getProjectUser(id);
		return successMessage(users);
	}

	public String getProjectTask(int id){
        List<Task> tasks = projectMapper.getProjectTask(id);
        List<JSONObject> resultTasks = new ArrayList<>();
        for(Task task : tasks){
            JSONArray relatedUserJsonArray = JSON.parseArray(task.getRelatedUser());
            JSONObject taskJsonObject = JSON.parseObject(JSON.toJSONString(task));
            taskJsonObject.put("relatedUser",relatedUserJsonArray);
            resultTasks.add(taskJsonObject);
        }
		return successMessage(resultTasks);
	}

	public String addProjectTask(int id, Project_AddProjectTask projectAddProjectTask){
		//add task
		Task task = new Task();
		task.setState(projectAddProjectTask.state);
		taskMapper.addTask(task);
		int task_id = task.getId();
		//add project_task
		projectMapper.addProjectTask(id, task_id);
		return successMessage();
	}

	public String removeProjectTask(int project_id, int task_id){
		//remove project_task
		projectMapper.removeProjectTask(project_id,task_id);
		//remove task
		taskMapper.removeTask(task_id);
		return successMessage();
	}

	public String getProjectFile(int id){
		return successMessage(projectMapper.getProjectFile(id));
	}

	public String getFile(int project_id, String file_name){
		return successMessage(fileMapper.getFile(project_id, file_name));
	}

	public String addProjectFile(int project_id, Project_AddProjectFile projectAddProjectFile){
		File file = new File();
		file.setId(UUID.randomUUID().toString().replaceAll("-",""));
		file.setName(projectAddProjectFile.name);
		file.setUrl(projectAddProjectFile.url);
		try {
			fileMapper.addFile(file);
		}catch (Exception e){
			System.out.println(e);
			return errorMessage("文件创建失败。");
		}
		String fileId = file.getId();
		//System.out.println("fileID:"+fileId);
		try {
			projectMapper.addProjectFile(project_id, fileId);
		}catch (Exception e){
			System.out.println(e);
			return errorMessage("文件置入项目失败。");
		}
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("fileId",fileId);
		return successMessage(jsonObject);
	}

	public String removeProjectFile(int project_id, String file_name){
		try {
			projectMapper.removeProjectFile(project_id, file_name);
		}catch (Exception e){
			System.out.println(e);
			return errorMessage("从项目中移除文件且删除文件失败。");
		}
		return successMessage();
	}
}
