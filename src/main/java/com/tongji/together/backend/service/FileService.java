package com.tongji.together.backend.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.tongji.together.backend.domain.File;
import com.tongji.together.backend.domain.Project;
import com.tongji.together.backend.domain.Task;
import com.tongji.together.backend.domain.User;
import com.tongji.together.backend.domain.relationDomain.Project_AddProject;
import com.tongji.together.backend.domain.relationDomain.Project_AddProjectFile;
import com.tongji.together.backend.domain.relationDomain.Project_AddProjectTask;
import com.tongji.together.backend.domain.relationDomain.Project_UpdateProject;
import com.tongji.together.backend.mapper.FileMapper;
import com.tongji.together.backend.mapper.ProjectMapper;
import com.tongji.together.backend.mapper.TaskMapper;
import com.tongji.together.backend.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * 文件业务Service
 * @author chaoszh
 * @since 2019-12-8
 */
@Service
public class FileService extends BaseService {
	@Autowired FileMapper fileMapper;

	public String getFile(int projectId, String fileName){
		return successMessage(fileMapper.getFile(projectId, fileName));
	}
}
