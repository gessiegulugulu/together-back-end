package com.tongji.together.backend.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tongji.together.backend.domain.Project;
import com.tongji.together.backend.domain.relationDomain.ProjectUser;
import com.tongji.together.backend.domain.User;
import com.tongji.together.backend.domain.UserLogin;
import com.tongji.together.backend.domain.relationDomain.User_AddUser;
import com.tongji.together.backend.domain.relationDomain.User_AddUserProject;
import com.tongji.together.backend.domain.relationDomain.User_LoginUser;
import com.tongji.together.backend.mapper.ProjectMapper;
import com.tongji.together.backend.mapper.UserLoginMapper;
import com.tongji.together.backend.mapper.UserMapper;
import com.tongji.together.backend.tool.SecurityUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * 用户业务Service
 * @author chaoszh
 * @since 2019-11-17
 */
@Service
public class UserService extends BaseService{
	@Autowired
	private UserMapper userMapper;
	@Autowired
	private UserLoginMapper userLoginMapper;
	@Autowired
	private ProjectMapper projectMapper;

	public String userLogin(User_LoginUser userLoginUser){
		UserLogin userLogin = new UserLogin(userLoginUser.email, SecurityUtil.getMd5(userLoginUser.password));
		Integer id;
		try {
			id = userLoginMapper.userLogin(userLogin);
		}catch (Exception e){
			System.out.println(e);
			return errorMessage("用户不存在。");
		}
		User user = userMapper.getUserWithId(id);
		return successMessage(user);
	}

	public String getUserWithId(int id){
		User user;
		try {
			user = userMapper.getUserWithId(id);
		}catch (Exception e){
			return errorMessage("用户ID不存在。");
		}
		if(user==null){
			return errorMessage("用户ID不存在。");
		}
		return successMessage(user);
	}

	public String getUserWithEmail(String email){
		User user;
		try {
			user = userMapper.getUserWithEmail(email);
		}catch (Exception e){
			return errorMessage("用户email不存在。");
		}
		if(user==null){
			return errorMessage("用户email不存在。");
		}
		return successMessage(user);
	}

	public String addUser(User_AddUser useraddUser){
		UserLogin userLogin = new UserLogin(useraddUser.email, SecurityUtil.getMd5(useraddUser.password));
		try {
			userLoginMapper.addUser(userLogin);
		}catch (Exception e){
			System.out.println(e);
			return errorMessage("邮箱已存在，请重试。");
		}
		int id = userLogin.getId();
		try {
			User user = new User();
			user.setId(id);
			user.setUserName(useraddUser.userName);
			userMapper.updateUser(user);
		}catch (Exception e){
			System.out.println(e);
			return errorMessage("更新用户信息失败。");
		}
		return successMessage();
	}

	public String updateUser(int id, User user){
		//JSONObject object = JSONObject.parseObject(json);
		//String userName = object.getString("userName");
		//String avatarUrl = object.getString("avatarUrl");
		//User user=new User(id,userName,avatarUrl);
		user.setId(id);
		userMapper.updateUser(user);
		return successMessage();
	}

	public String getUserProject(int id){
		List<Project> projects=userMapper.getUserProject(id);
		//sort
		projects.sort(new Comparator<Project>() {
			@Override
			public int compare(Project o1, Project o2) {
				return o1.getLastModifiedTime().compareTo(o2.getLastModifiedTime());
			}
		});
		DateFormat dataformat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		List<JSONObject> json_projects=new ArrayList<>();
		for(int i=0;i<projects.size();i++){
			String json = JSON.toJSONString(projects.get(i));
			JSONObject jsonObject = JSONObject.parseObject(json);
			//last modified time
			long lastModifiedTime = jsonObject.getLong("lastModifiedTime");
			String dateString = dataformat.format(lastModifiedTime);
			jsonObject.replace("lastModifiedTime", dateString);
			//creator name
			User adminUser = projectMapper.getProjectUseradmin(projects.get(i).getId());
			JSONObject user_jsonObject = JSONObject.parseObject(JSON.toJSONString(adminUser));
			jsonObject.put("adminUser",user_jsonObject);
			json_projects.add(jsonObject);
		}
		return successMessage(json_projects);
	}

	public String addUserProject(int userId, int projectId, User_AddUserProject userAddUserProject){
		try{
			if(userAddUserProject.inCharge.equals(null))userAddUserProject.inCharge=false;
			userMapper.addUserProject(userId, projectId, userAddUserProject.inCharge);
		}catch(Exception e){
			System.out.println(e);
			errorMessage("不存在用户或项目，用户加入失败。");
		}
		return successMessage();
	}

	public String removeUserProject(int userId, int projectId){
		ProjectUser projectUser = userMapper.getUserProjectRelation(userId, projectId);
		//用户不是项目成员
		if(projectUser==null){
			return errorMessage("This user is not a participant of the project!");
		}
		//用户是管理员，不能删除
		else if(projectUser.inCharge==true){
			return errorMessage("Can't remove owner from project!");
		}
		//用户不是管理员且是项目成员
		else {
			userMapper.removeUserProject(userId,projectId);
			return successMessage();
		}
	}
}
