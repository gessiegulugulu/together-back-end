package com.tongji.together.backend.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.gson.JsonObject;
import com.tongji.together.backend.domain.Task;
import com.tongji.together.backend.domain.relationDomain.Task_UpdateTask;
import com.tongji.together.backend.mapper.TaskMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;

/**
 * 任务业务Service
 * @author chaoszh
 * @since 2019-11-17
 */
@Service
public class TaskService extends BaseService{
	@Autowired
	private TaskMapper taskMapper;

	public String getTask(int id){
        Task task = taskMapper.getTask(id);
        if(task==null){
        	return errorMessage("任务ID不存在。");
        }
        JSONArray relatedUserJsonArray = JSON.parseArray(task.getRelatedUser());
        JSONObject taskJsonObject = JSON.parseObject(JSON.toJSONString(task));
        taskJsonObject.put("relatedUser",relatedUserJsonArray);
		return successMessage(taskJsonObject);
	}

	public String updateTask(int id, Task_UpdateTask taskUpdateTask){
		System.out.println(taskUpdateTask.toString());
		Task task = new Task(
				taskUpdateTask.title,
				taskUpdateTask.description,
				taskUpdateTask.state,
				JSON.toJSONString(taskUpdateTask.relatedUser),
				taskUpdateTask.timeStart,
				taskUpdateTask.timeEnd
		);
		taskMapper.updateTask(id, task);
		return successMessage();
	}
}
