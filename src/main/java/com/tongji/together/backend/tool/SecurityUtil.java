package com.tongji.together.backend.tool;

import java.math.BigInteger;
import java.security.MessageDigest;

/**
 * 生成MD5码工具
 * @author chaoszh
 * @since 2019-12-08
 */
public class SecurityUtil {
	public static String getMd5(String input) {
		MessageDigest md;
		String result = "";
		try {
			md = MessageDigest.getInstance("md5");
			md.update(input.getBytes());
			result = new BigInteger(1, md.digest()).toString(16);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("md5生成摘要错误");
		}
		return result;
	}
}
