package com.tongji.together.backend.mapper;

import com.tongji.together.backend.domain.Project;
import com.tongji.together.backend.domain.relationDomain.ProjectUser;
import com.tongji.together.backend.domain.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用户Mapper
 * @author chaoszh
 * @since 2019-11-16
 */
public interface UserMapper {

	/**
	 * 通过id获取用户信息
	 * */
	User getUserWithId(int id);

	/**
	 * 通过email获取用户信息
	 * */
	User getUserWithEmail(String email);

	/**
	 * 更改用户信息
	 * */
	boolean updateUser(@Param("user") User user);

	/**
	 * 获取用户参加的所有项目
	 * */
	List<Project> getUserProject(int id);

	/**
	 * 用户参加项目
	 * */
	void addUserProject(@Param("user_id")int user_id, @Param("project_id")int project_id, @Param("in_charge") boolean inCharge);

	/**
	 * 判断用户是否参加了项目
	 * */
	ProjectUser getUserProjectRelation(@Param("user_id")int user_id, @Param("project_id")int project_id);

	/**
	 * 用户退出项目
	 * */
	void removeUserProject(@Param("user_id")int user_id, @Param("project_id")int project_id);
}
