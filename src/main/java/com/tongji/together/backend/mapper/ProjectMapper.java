package com.tongji.together.backend.mapper;

import com.tongji.together.backend.domain.File;
import com.tongji.together.backend.domain.Project;
import com.tongji.together.backend.domain.Task;
import com.tongji.together.backend.domain.User;
import com.tongji.together.backend.domain.relationDomain.Project_GetProjectUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 项目Mapper
 * @author chaoszh
 * @since 2019-11-16
 */
public interface ProjectMapper {
	int addProject(@Param("project") Project project);
	Project getProject(int id);
	void updateProject(@Param("id") int id, @Param("project")Project project);
	void removeProject(int id);
	void removeProjectUser(int id);
	List<Project_GetProjectUser> getProjectUser(int id);
	User getProjectUseradmin(int id);
	List<Task> getProjectTask(int id);
	void addProjectTask(@Param("project_id")int project_id, @Param("task_id")int task_id);
	void removeProjectTask(@Param("project_id")int project_id, @Param("task_id")int task_id);
	List<File> getProjectFile(int id);
	void addProjectFile(@Param("project_id")int project_id, @Param("file_id")String file_id);
	void removeProjectFile(@Param("project_id")int project_id, @Param("file_name")String file_name);
}
