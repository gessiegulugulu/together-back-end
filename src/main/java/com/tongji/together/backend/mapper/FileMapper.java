package com.tongji.together.backend.mapper;

import com.tongji.together.backend.domain.File;
import com.tongji.together.backend.domain.Task;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 文件Mapper
 * @author chaoszh
 * @since 2019-12-8
 */
public interface FileMapper {
	void addFile(@Param("file") File file);
	void removeFile(@Param("project_id") int projectId, @Param("file_name")String fileName);
	List<File> getFile(@Param("project_id") int projectId, @Param("file_name")String fileName);
}
