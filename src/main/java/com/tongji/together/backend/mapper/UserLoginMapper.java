package com.tongji.together.backend.mapper;

import com.tongji.together.backend.domain.UserLogin;
import org.apache.ibatis.annotations.Param;

/**
 * 用户登录注册Mapper
 * @author chaoszh
 * @since 2019-11-16
 */
public interface UserLoginMapper {
	/**
	 * 添加用户
	 * */
	boolean addUser(@Param("userLogin")UserLogin userLogin);

	/**
	 * 用户登录
	 * */
	int userLogin(@Param("userLogin")UserLogin userLogin);
}
