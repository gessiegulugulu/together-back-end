package com.tongji.together.backend.mapper;

import com.tongji.together.backend.domain.Task;
import org.apache.ibatis.annotations.Param;

/**
 * 任务Mapper
 * @author chaoszh
 * @since 2019-11-16
 */
public interface TaskMapper {
	int addTask(@Param("task") Task task);
	void removeTask(int id);
	Task getTask(int id);
	void updateTask(@Param("id")int id, @Param("task")Task task);
}
