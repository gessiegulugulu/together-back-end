package com.tongji.together.backend.configuration;

import com.tencent.cloud.CosStsClient;
import org.json.JSONObject;

import java.util.TreeMap;

/**
 * 获取腾讯云对象存储认证
 * @author chaoszh
 * @since 2019-11-17
 */
public class CosConfiguration {
	public static JSONObject getCredential(){
		TreeMap<String, Object> config = new TreeMap<String, Object>();
		try {
			config.put("SecretId", "AKIDEEc9crcOmOq33ntXFnAH8mAwRlqhcgpk");
			config.put("SecretKey", "vXy98sNfhU1OTLyUp1xd9Q4PUzDC2wWt");
			// 换成您的 bucket
			config.put("bucket", "publicmessage-1259081301");
			// 换成 bucket 所在地区
			config.put("region", "ap-shanghai");

			// 临时密钥有效时长，单位是秒，默认1800秒，最长可设定有效期为7200秒
			config.put("durationSeconds", 7200);
			config.put("allowPrefix", "*");
			// 密钥的权限列表。简单上传、表单上传和分片上传需要以下的权限，其他权限列表请看 https://cloud.tencent.com/document/product/436/31923
			String[] allowActions = new String[] {
					// 简单上传
					"name/cos:PutObject",
					// 表单上传、小程序上传
					"name/cos:PostObject",
					// 分片上传
					"name/cos:InitiateMultipartUpload",
					"name/cos:ListMultipartUploads",
					"name/cos:ListParts",
					"name/cos:UploadPart",
					"name/cos:CompleteMultipartUpload"
			};
			config.put("allowActions", allowActions);
			JSONObject credential = CosStsClient.getCredential(config);
			return credential;
		} catch (Exception e) {
			throw new IllegalArgumentException("No valid Secret!");
		}
	}
}
