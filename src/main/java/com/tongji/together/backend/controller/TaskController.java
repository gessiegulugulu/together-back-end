package com.tongji.together.backend.controller;

import com.tongji.together.backend.domain.relationDomain.Project_AddProjectTask;
import com.tongji.together.backend.domain.relationDomain.Task_UpdateTask;
import com.tongji.together.backend.service.ProjectService;
import com.tongji.together.backend.service.TaskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 任务Controllers
 * @author chaoszh
 * @since 2019-11-17
 */
@Api(tags = "任务模块")
@RestController
public class TaskController {
	@Autowired
	private TaskService taskService;

	@Autowired
	private ProjectService projectService;

	@ApiOperation("获取任务信息")
	@GetMapping("/v1/task/{id}")
	public String getTask(@PathVariable int id){
		return taskService.getTask(id);
	}

	@ApiOperation("更新任务信息")
	@PostMapping("/v1/task/{id}")
	public String updateTask(@PathVariable int id, @RequestBody Task_UpdateTask task){
		return taskService.updateTask(id, task);
	}

	@ApiOperation("获取项目中所有任务")
	@GetMapping("/v1/project/{id}/task")
	public String getProjectTask(@PathVariable int id){
		return projectService.getProjectTask(id);
	}

	@ApiOperation("新建项目的任务")
	@PostMapping("/v1/project/{id}/task")
	public String addProjectTask(@PathVariable int id, @RequestBody Project_AddProjectTask task){
		return projectService.addProjectTask(id, task);
	}

	@ApiOperation("删除项目的任务")
	@DeleteMapping("/v1/project/{project_id}/task/{task_id}")
	public String removeProjectTask(@PathVariable int project_id, @PathVariable int task_id){
		return projectService.removeProjectTask(project_id, task_id);
	}
}
