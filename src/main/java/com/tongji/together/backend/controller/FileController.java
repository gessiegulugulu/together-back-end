package com.tongji.together.backend.controller;

import com.tongji.together.backend.configuration.CosConfiguration;
import com.tongji.together.backend.domain.relationDomain.Project_AddProjectFile;
import com.tongji.together.backend.service.FileService;
import com.tongji.together.backend.service.ProjectService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * 文件Controllers
 * @author chaoszh
 * @since 2019-12-08
 */

@Api(tags="文件模块")
@RestController
public class FileController {
	public CosConfiguration cosConfiguration;

	@Autowired
	private FileService fileService;

	@Autowired
	private ProjectService projectService;

	@ApiOperation(value = "获取腾讯云COS密钥")
	@GetMapping("/v1/file/credential")
	public String getCredential(){
		return cosConfiguration.getCredential().toString();
	}

	@ApiOperation("获取项目的所有文件")
	@GetMapping("/v1/project/{id}/file")
	public String getProjectFile(@PathVariable int id){
		return projectService.getProjectFile(id);
	}

	@ApiOperation("新建项目的文件")
	@PostMapping("/v1/project/{id}/file")
	public String addProjectFile(@PathVariable int id, @RequestBody Project_AddProjectFile file){
		return projectService.addProjectFile(id,file);
	}

	@ApiOperation("获取项目某一文件")
	@GetMapping("/v1/project/{project_id}/file/{file_name}")
	public String addProjectFile(@PathVariable int project_id, @PathVariable String file_name){
		return projectService.getFile(project_id,file_name);
	}

	@ApiOperation("删除项目的文件")
	@DeleteMapping("/v1/project/{project_id}/file/{file_name}")
	public String removeProjectFile(@PathVariable int project_id, @PathVariable String file_name){
		return projectService.removeProjectFile(project_id,file_name);
	}
}
