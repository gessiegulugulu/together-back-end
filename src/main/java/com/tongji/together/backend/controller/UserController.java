package com.tongji.together.backend.controller;

import com.tongji.together.backend.domain.User;
import com.tongji.together.backend.domain.relationDomain.User_AddUser;
import com.tongji.together.backend.domain.relationDomain.User_AddUserProject;
import com.tongji.together.backend.domain.relationDomain.User_LoginUser;
import com.tongji.together.backend.service.ProjectService;
import com.tongji.together.backend.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 用户Controllers
 * @author chaoszh
 * @since 2019-11-17
 */

@Api(tags = "用户模块")
@RestController
public class UserController {
	@Autowired
	public UserService userService;
	@Autowired
	public ProjectService projectService;

	@ApiOperation("根据id查询用户")
	@GetMapping("/v1/user/id/{id}")
	public String getUserWithId(@PathVariable int id){
		return userService.getUserWithId(id);
	}

	@ApiOperation("根据email查询用户")
	@GetMapping("/v1/user/email/{email}")
	public String getUserWithId(@PathVariable String email){
		return userService.getUserWithEmail(email);
	}

	@ApiOperation(value = "注册/新建用户", notes = "PARAMS:userName;password;email")
	@PostMapping("/v1/user")
	public String addUser(@RequestBody User_AddUser user){
		return userService.addUser(user);
	}

	@ApiOperation(value = "用户登录")
	@PostMapping("/v1/user/login")
	public String userLogin(@RequestBody User_LoginUser user){
		return userService.userLogin(user);
	}

	@ApiOperation(value = "更改用户信息")
	@PostMapping("/v1/user/{id}")
	public String updateUser(@PathVariable int id, @RequestBody User user){
		return userService.updateUser(id, user);
	}

	@ApiOperation("获取用户参加的所有项目")
	@GetMapping("/v1/user/{id}/project")
	public String getUserProject(@PathVariable int id){
		return userService.getUserProject(id);
	}

	@ApiOperation("用户参加项目")
	@PostMapping("v1/user/{user_id}/project/{project_id}")
	public String addUserProject(@PathVariable int user_id, @PathVariable int project_id, @RequestBody User_AddUserProject userProject){
		return userService.addUserProject(user_id,project_id,userProject);
	}

	@ApiOperation("用户退出项目")
	@DeleteMapping("v1/user/{user_id}/project/{project_id}")
	public String removeUserProject(@PathVariable int user_id, @PathVariable int project_id){
		return userService.removeUserProject(user_id,project_id);
	}

	@ApiOperation("获取项目中所有成员")
	@GetMapping("/v1/project/{id}/user")
	public String getProjectUser(@PathVariable int id){
		return projectService.getProjectUser(id);
	}
}
