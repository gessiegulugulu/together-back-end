package com.tongji.together.backend.controller;

import com.tongji.together.backend.domain.Project;
import com.tongji.together.backend.domain.relationDomain.Project_AddProject;
import com.tongji.together.backend.domain.relationDomain.Project_AddProjectFile;
import com.tongji.together.backend.domain.relationDomain.Project_AddProjectTask;
import com.tongji.together.backend.domain.relationDomain.Project_UpdateProject;
import com.tongji.together.backend.mapper.ProjectMapper;
import com.tongji.together.backend.service.ProjectService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.sql.Timestamp;

/**
 * 项目Controllers
 * @author chaoszh
 * @since 2019-11-17
 */
@Api(tags="项目模块")
@RestController
public class ProjectController {
	@Autowired
	private ProjectService projectService;

	@ApiOperation("添加新项目")
	@PostMapping("/v1/project")
	public String addProject(@RequestBody Project_AddProject project){
		return projectService.addProject(project);
	}

	@ApiOperation("获取项目信息")
	@GetMapping("/v1/project/{id}")
	public String getProject(@PathVariable int id){
		return projectService.getProject(id);
	}

	@ApiOperation("修改项目信息")
	@PostMapping("/v1/project/{id}")
	public String updateProject(@PathVariable int id, @RequestBody Project_UpdateProject project){
		return projectService.updateProject(id, project);
	}

	@ApiOperation("删除项目")
	@DeleteMapping("/v1/project/{id}")
	public String removeProject(@PathVariable int id){
		return projectService.removeProject(id);
	}
}
