package com.tongji.together.backend.controller;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用于测试的Controllers
 * @author chaoszh
 * @since 2019-11-17
 */

@Api(tags="Hello World")
@RestController
public class HelloController {
	@GetMapping("/hello")
	public String hello(){
		return "hello.";
	}
}
