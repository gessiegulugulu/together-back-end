package com.tongji.together.backend.domain.relationDomain;

import io.swagger.annotations.ApiModelProperty;

import java.sql.Timestamp;
import java.util.List;

public class Project_UpdateProject {
	@ApiModelProperty("项目名称")
	public String projectName;
	@ApiModelProperty(value = "项目状态列表",example = "[\"state1\",\"state2\",\"state3\"]")
	public List<String> states;

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public List<String> getStates() {
		return states;
	}

	public void setStates(List<String> states) {
		this.states = states;
	}
}
