package com.tongji.together.backend.domain.relationDomain;

import io.swagger.annotations.ApiModelProperty;

import javax.swing.*;
import java.sql.Timestamp;

/**
 * 强关系模型
 * @author chaoszh
 * @since 2019-12-07
 */
public class Project_AddProject {
	@ApiModelProperty("项目名称")
	public String projectName;
	@ApiModelProperty("项目创建者ID")
	public int creatorId;

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public int getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(int creatorId) {
		this.creatorId = creatorId;
	}
}
