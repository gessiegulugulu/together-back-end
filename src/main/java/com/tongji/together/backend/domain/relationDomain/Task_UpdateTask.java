package com.tongji.together.backend.domain.relationDomain;

import io.swagger.annotations.ApiModelProperty;

import java.sql.Timestamp;
import java.util.List;

/**
 * 强关系模型
 * @author chaoszh
 * @since 2019-12-07
 */
public class Task_UpdateTask {
	@ApiModelProperty("任务标题")
	public String title;
	@ApiModelProperty("任务描述")
	public String description;
	@ApiModelProperty("任务状态")
	public String state;
	@ApiModelProperty(value = "任务相关用户",example = "[\"user1\",\"user2\",\"user3\"]")
	public List<String> relatedUser;
	@ApiModelProperty(value = "任务开始时间",example = "158345672")
	public Timestamp timeStart;
	@ApiModelProperty(value = "任务结束时间",example = "158345672")
	public Timestamp timeEnd;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public List<String> getRelatedUser() {
		return relatedUser;
	}

	public void setRelatedUser(List<String> relatedUser) {
		this.relatedUser = relatedUser;
	}

	public Timestamp getTimeStart() {
		return timeStart;
	}

	public void setTimeStart(Timestamp timeStart) {
		this.timeStart = timeStart;
	}

	public Timestamp getTimeEnd() {
		return timeEnd;
	}

	public void setTimeEnd(Timestamp timeEnd) {
		this.timeEnd = timeEnd;
	}

	@Override
	public String toString() {
		return "Task_UpdateTask{" +
				"title='" + title + '\'' +
				", description='" + description + '\'' +
				", state='" + state + '\'' +
				", relatedUser=" + relatedUser +
				", timeStart=" + timeStart +
				", timeEnd=" + timeEnd +
				'}';
	}
}
