package com.tongji.together.backend.domain.relationDomain;

import io.swagger.annotations.ApiModelProperty;

import java.sql.Timestamp;

/**
 * 强关系模型
 * @author chaoszh
 * @since 2019-12-07
 */
public class Project_AddProjectTask {
	@ApiModelProperty("任务状态")
	public String state;

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
}
