package com.tongji.together.backend.domain.relationDomain;

import io.swagger.annotations.ApiModelProperty;

/**
 * 强关系模型
 * @author chaoszh
 * @since 2019-12-07
 */
public class User_LoginUser {
	@ApiModelProperty("用户邮箱地址")
	public String email;
	@ApiModelProperty("用户密码")
	public String password;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
