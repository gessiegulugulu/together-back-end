package com.tongji.together.backend.domain;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 用户基础Domain
 * @author chaoszh
 * @since 2019-11-17
 */
public class User implements Serializable {
	@ApiModelProperty("用户ID")
	private int id;
	@ApiModelProperty("用户名")
	private String userName;
	@ApiModelProperty("用户头像Url")
	private String avatarUrl;

	public User(){};
	public User(int id, String userName, String avatarUrl){
		this.id=id;
		this.userName=userName;
		this.avatarUrl=avatarUrl;
	}

	@Override
	public String toString() {
		return "{" +
				"id=" + id +
				", userName='" + userName + '\'' +
				", avatarUrl='" + avatarUrl + '\'' +
				'}';
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getAvatarUrl() {
		return avatarUrl;
	}

	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}
}
