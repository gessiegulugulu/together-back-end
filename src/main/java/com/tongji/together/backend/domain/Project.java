package com.tongji.together.backend.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.sql.Timestamp;

/**
 * 项目基础Domain
 * @author chaoszh
 * @since 2019-11-17
 */
public class Project {
	@ApiModelProperty("项目ID")
	private int id;
	@ApiModelProperty("项目名称")
	private String projectName;
	@ApiModelProperty("项目状态列表")
	private String states;
	@ApiModelProperty("项目最近修改时间")
	private Timestamp lastModifiedTime;

	public Project(){}

	public Project(String projectName, Timestamp lastModifiedTime) {
		this.projectName = projectName;
		this.lastModifiedTime = lastModifiedTime;
	}

	public Project(String projectName, String states, Timestamp lastModifiedTime) {
		this.projectName = projectName;
		this.states = states;
		this.lastModifiedTime = lastModifiedTime;
	}

	@Override
	public String toString() {
		return "{" +
				"id=" + id +
				", projectName='" + projectName + '\'' +
				", states='" + states + '\'' +
				", lastModifiedTime=" + lastModifiedTime +
				'}';
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getStates() {
		return states;
	}

	public void setStates(String states) {
		this.states = states;
	}

	public Timestamp getLastModifiedTime() {
		return lastModifiedTime;
	}

	public void setLastModifiedTime(Timestamp lastModifiedTime) {
		this.lastModifiedTime = lastModifiedTime;
	}
}
