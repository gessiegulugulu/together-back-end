package com.tongji.together.backend.domain.relationDomain;

/**
 * 强关系模型
 * @author chaoszh
 * @since 2019-12-07
 */
public class ProjectUser {
	public int userId;
	public int projectId;
	public Boolean inCharge;

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getProjectId() {
		return projectId;
	}

	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}

	public Boolean getInCharge() {
		return inCharge;
	}

	public void setInCharge(Boolean inCharge) {
		this.inCharge = inCharge;
	}
}
