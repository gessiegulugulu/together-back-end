package com.tongji.together.backend.domain;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 任务基础Domain
 * @author chaoszh
 * @since 2019-11-17
 */
public class Task implements Serializable {
	@ApiModelProperty("任务ID")
	private int id;
	@ApiModelProperty("任务标题")
	private String title;
	@ApiModelProperty("任务描述")
	private String description;
	@ApiModelProperty("任务状态")
	private String state;
	@ApiModelProperty(value = "任务相关用户",example = "[\"user1\",\"user2\",\"user3\"]")
	private String relatedUser;
	@ApiModelProperty("任务开始时间")
	private Timestamp timeStart;
	@ApiModelProperty("任务结束时间")
	private Timestamp timeEnd;

	public Task(){}

	public Task(String title, String description, String state, String relatedUser, Timestamp timeStart, Timestamp timeEnd) {
		this.title = title;
		this.description = description;
		this.state = state;
		this.relatedUser = relatedUser;
		this.timeStart = timeStart;
		this.timeEnd = timeEnd;
	}

	@Override
	public String toString() {
		return "Task{" +
				"id=" + id +
				", title='" + title + '\'' +
				", description='" + description + '\'' +
				", state='" + state + '\'' +
				", relatedUser='" + relatedUser + '\'' +
				", timeStart=" + timeStart +
				", timeEnd=" + timeEnd +
				'}';
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getRelatedUser() {
		return relatedUser;
	}

	public void setRelatedUser(String relatedUser) {
		this.relatedUser = relatedUser;
	}

	public Timestamp getTimeStart() {
		return timeStart;
	}

	public void setTimeStart(Timestamp timeStart) {
		this.timeStart = timeStart;
	}

	public Timestamp getTimeEnd() {
		return timeEnd;
	}

	public void setTimeEnd(Timestamp timeEnd) {
		this.timeEnd = timeEnd;
	}
}
