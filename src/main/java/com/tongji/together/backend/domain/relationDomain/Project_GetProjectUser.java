package com.tongji.together.backend.domain.relationDomain;

import io.swagger.annotations.ApiModelProperty;

public class Project_GetProjectUser {
	@ApiModelProperty("用户ID")
	private int id;
	@ApiModelProperty("用户名")
	private String userName;
	@ApiModelProperty("用户头像Url")
	private String avatarUrl;
	@ApiModelProperty("用户是否为项目负责人")
	private Boolean inCharge;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getAvatarUrl() {
		return avatarUrl;
	}

	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}

	public Boolean getInCharge() {
		return inCharge;
	}

	public void setInCharge(Boolean inCharge) {
		this.inCharge = inCharge;
	}
}
