package com.tongji.together.backend.domain.relationDomain;


import io.swagger.annotations.ApiModelProperty;

/**
 * 强关系模型
 * @author chaoszh
 * @since 2019-12-07
 */
public class User_AddUserProject {
	@ApiModelProperty("用户是否为项目管理者")
	public Boolean inCharge;

	public Boolean getInCharge() {
		return inCharge;
	}

	public void setInCharge(Boolean inCharge) {
		this.inCharge = inCharge;
	}
}
