package com.tongji.together.backend.domain;

import java.io.Serializable;

/**
 * 用户登陆表Domain
 * @author chaoszh
 * @since 2019-11-17
 */
public class UserLogin implements Serializable {
	private int id;
	private String email;
	private String password;

	public UserLogin(String email, String password) {
		this.email = email;
		this.password = password;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
