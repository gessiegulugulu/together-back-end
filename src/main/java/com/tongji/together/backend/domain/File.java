package com.tongji.together.backend.domain;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 文件基础Domain
 * @author chaoszh
 * @since 2019-12-8
 */
public class File implements Serializable {
	@ApiModelProperty("文件ID")
	private String id;
	@ApiModelProperty("文件名称")
	private String name;
	@ApiModelProperty("文件Url")
	private String url;
	@ApiModelProperty("文件上传时间")
	private Timestamp createTime;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}
}
