package com.tongji.together.backend.domain.relationDomain;

import io.swagger.annotations.ApiModelProperty;

/**
 * 强关系模型
 * @author chaoszh
 * @since 2019-12-07
 */
public class Project_AddProjectFile {
	@ApiModelProperty("文件名称")
	public String name;
	@ApiModelProperty("文件Url")
	public String url;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
